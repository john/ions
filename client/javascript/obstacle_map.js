/**
 * @param {Number} width
 * @param {Number} height
 */
ObstacleMap = function(width, height) {
    this.width = width;
    this.height = height;
    this.data = new BitArray(this.width * this.height);
}

/**
 * @param {Number} x
 * @param {Number} y
 * @return {boolean}
 */
ObstacleMap.prototype.getAt = function(x, y) {
    if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
        return this.data.getAt(y * this.width + x);
    }
    return true;
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Boolean} val
 */
ObstacleMap.prototype.setAt = function(x, y, val) {
    if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
        this.data.setAt(y * this.width + x, val);
    }
}
