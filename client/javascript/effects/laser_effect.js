LaserEffect = function(def) {
    this.def = def;
    this.lifetime = def.lifetime;
}

LaserEffect.prototype.draw = function(scale, ctx) {
    ctx.beginPath();
    ctx.strokeStyle = this.def.color;
    ctx.lineCap = 'round';
    ctx.lineWidth = 1 * scale;
    ctx.moveTo(this.def.from[0] * scale, this.def.from[1] * scale);
    ctx.lineTo(this.def.to[0] * scale, this.def.to[1] * scale);
    ctx.stroke();
}
