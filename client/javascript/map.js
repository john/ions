Map = function(canvas, sprite_pool) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext("2d");

    this.sprite_pool = sprite_pool;

    //internal grid dimensions
    this.width = 10;
    this.height = 20;
    this.draw_heigth = this.canvas.getAttribute('height');
    this.draw_width = this.canvas.getAttribute('width');

    this.spawn_point = [0, 0];
    this.end_point = [0, 0];

    this.build_grid = [];

    this.data = [];
    this.obstacle_map = new ObstacleMap(this.draw_width, this.draw_heigth);

    this.block_size = 36;
    this.half_block_size = this.block_size / 2;

    for (var i = 0; i < this.height; i++) {
        var row = []
        for (var j = 0; j < this.width; j++) {
            row.push(null);
        };
        this.data.push(row);
    };
}

/**
 * @param {String} input
 */
Map.prototype.fill = function(input) {

    this.ctx.clearRect(0, 0, this.draw_width, this.draw_heigth);
    this.input = input;
    var rows = input.split(':');

    // draw inflated collision map
    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < rows[i].length; j++) {
            if (rows[i].substr(j, 1) == "X") {
                var x = j * this.block_size + this.half_block_size;
                var y = i * this.block_size + this.half_block_size;
                this.ctx.beginPath();
                this.ctx.arc(x, y, 35, 0, 2 * Math.PI, false);
                this.ctx.fillStyle = 'red';
                this.ctx.fill();
            }
        };
    };

    // update map of blocked pixels
    this.updateObstacleMap();

    //draw default blocks
    for (var i = 0; i < rows.length; i++) {
        this.build_grid[i] = [];
        for (var j = 0; j < rows[i].length; j++) {
            if (rows[i].substr(j, 1) == "X") { // build area
                this.build_grid[i][j] = true;
                this.add(j, i, this.sprite_pool.get("img/block.png"));
            } else {
                this.build_grid[i][j] = false;
            }
        };
    };

    //draw spawn and end point
    for (var i = 0; i < rows.length; i++) {
        for (var j = 0; j < rows[i].length; j++) {
            if (rows[i].substr(j, 1) == "S") { // start point
                this.spawn_point = [
                    j * this.block_size + this.half_block_size,
                    i * this.block_size + this.half_block_size
                ];
            }
            if (rows[i].substr(j, 1) == "E") { // end point
                this.end_point = [
                    j * this.block_size + this.half_block_size,
                    i * this.block_size + this.half_block_size
                ];
            }
        };
    };
}

Map.prototype.updateObstacleMap = function() {
    var background = this.ctx.getImageData(0,0,this.draw_width, this.draw_heigth);
    for (var i = 0; i < this.draw_heigth; i++) {
        for (var j = 0; j < this.draw_width; j++) {
            if (background.data[(i * this.draw_width + j) * 4] != 0) {
                this.obstacle_map.setAt(j, i, true);
            }
        };
    };
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Image} sprite
 */
Map.prototype.add = function(x, y, sprite) {
    this.data[y][x] = sprite;
}

/**
 * @param {Number} scale
 */
Map.prototype.draw = function(scale) {
    this.ctx.clearRect(0, 0, this.draw_width, this.draw_heigth);

    // var rows = this.input.split(':');

    // for (var i = 0; i < rows.length; i++) {
    //     for (var j = 0; j < rows[i].length; j++) {
    //         if (rows[i].substr(j, 1) == "X") {
    //             var x = (j * this.block_size + this.half_block_size) * scale;
    //             var y = (i * this.block_size + this.half_block_size) * scale;
    //             this.ctx.beginPath();
    //             this.ctx.arc(x, y, 35 * scale, 0, 2 * Math.PI, false);
    //             this.ctx.fillStyle = 'red';
    //             this.ctx.fill();
    //         }
    //     };
    // };


    for (var i = 0; i < 20; i++) {
        for (var j = 0; j < 10; j++) {
            this.ctx.drawImage(
                this.sprite_pool.get("img/noise.png"),
                j * this.block_size * scale,
                i * this.block_size * scale,
                36 * scale,
                36 * scale
            );
        };
    };

    for (var i = 0; i < this.height; i++) {
        for (var j = 0; j < this.width; j++) {
            var e = this.data[i][j];
            if (e) {
                this.ctx.drawImage(
                    e,
                    j * this.block_size * scale,
                    i * this.block_size * scale,
                    36 * scale,
                    36 * scale
                );
            }
        };
    };
}
