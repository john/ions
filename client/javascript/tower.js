/**
 * @param {Number} x
 * @param {Number} y
 * @param {Tower} def
 */
Tower = function(x, y, def) {
    this.level = 0;
    this.def = def;
    this.position = [x, y];
    this.rotation = false;
    this.attack_interval_counter = 0;
    this.buildGraphics();
}

Tower.prototype.getPos = function() {
    return this.position;
}

Tower.prototype.upgrade = function() {
    if (this.def.levels[this.level + 1]) {
        this.level++;
        this.buildGraphics();
    }
}

Tower.prototype.buildGraphics = function() {
    if (this.def.levels[this.level].rotates) {
        this.graphics = new RotatingSprite(
            36,
            36,
            Game.sprite_pool.get(this.def.levels[this.level].sprites[0])
        );
    } else {
        this.graphics = new AnimatedSprite(
            36,
            36,
            Game.sprite_pool.getList(this.def.levels[this.level].sprites),
            this.def.levels[this.level].spriteInterval,
            2,
            true
        );
    }
}

/**
 * @return {boolean}
 */
Tower.prototype.canUpgrade = function() {
    return this.level < this.def.levels.length - 1;
}

/**
 * @param {Mob} target
 */
Tower.prototype.updateRotation = function(target) {
    var deltaY = -this.position[1] - -target[1];
    var deltaX = this.position[0] - target[0];
    this.rotation = Math.floor((Math.atan2(deltaX, deltaY) * 180 / Math.PI) + 180);
}

Tower.prototype.attack = function() {
    if (this.attack_interval_counter == 0) {
        var a = this.def.levels[this.level].attack;
        switch(a.type) {
            case 'laser':
                var target = Game.getNearestMob(this.position[0], this.position[1], a.range);
                if (target) {
                    target.addEffect(new DirectDamageEffect(1, a.damage));
                    Game.effect_layer.add(new LaserEffect({
                        color: a.color,
                        lifetime: 2,
                        from: [this.position[0], this.position[1]],
                        to: [target.getPos()[0], target.getPos()[1]]
                    }));
                    this.updateRotation(target.getPos());
                }
            break;
            case 'bullets':
                var target = Game.getNearestMob(this.position[0], this.position[1], a.range);
                if (target) {
                    var from = this.getPos();
                    var to = target.getPos();
                    var delay = Math.round(distance([from[0], from[1]], [to[0], to[1]]) / a.bullet_speed);

                    target.addEffect(new DirectDamageEffect(1, a.damage, delay));
                    Game.spawnProjectile(new Projectile(
                        from,
                        target,
                        a.bullet_speed,
                        new StaticSprite(2, 2, Game.sprite_pool.get('img/bullet.png'))
                    ));
                    this.updateRotation(target.getFuturePos(delay));
                }
            break;
            case 'bomb':
                var target = Game.getNearestMob(this.position[0], this.position[1], a.range);
                if (target) {

                    var from = this.getPos();
                    var to = target.getPos();
                    var delay = Math.round(distance([from[0], from[1]], [to[0], to[1]]) / a.bullet_speed);

                    var p = target.getPos();
                    var targets = Game.getMobsInRange(p[0], p[1], a.splash);

                    for (var i = 0; i < targets.length; i++) {
                        targets[i].addEffect(new DirectDamageEffect(1, a.damage, delay));
                    };

                    Game.spawnProjectile(new Projectile(
                        from,
                        target,
                        a.bullet_speed,
                        new StaticSprite(2, 2, Game.sprite_pool.get('img/bullet.png'))
                    ));

                    Game.effect_layer.add(new SpriteEffect({
                        lifetime: 10 + delay,
                        target: new DummyTarget(target.getFuturePos(delay)),
                        graphics: new RandomizedSprite(
                            40,
                            40,
                            Game.sprite_pool.getList([
                                "img/explosion0.png",
                                "img/explosion1.png",
                                "img/explosion2.png"

                            ])
                        ),
                        delay: delay
                    }));

                    this.updateRotation(target.getFuturePos(delay));
                }
            break;
            case 'fire_wave':
                var targets = Game.getMobsInRange(this.position[0], this.position[1], a.range);
                if (targets.length) {
                    for (var i = 0; i < targets.length; i++) {
                        targets[i].addEffect(new FireEffect(a.interval, a.damage));
                    };

                    Game.effect_layer.add(new SpriteEffect({
                        lifetime: 10,
                        target: this,
                        graphics: new AnimatedSprite(
                            80,
                            80,
                            Game.sprite_pool.getList([
                                "img/red_wave_0.png",
                                "img/red_wave_1.png",
                                "img/red_wave_2.png",
                                "img/red_wave_3.png",
                                "img/red_wave_4.png",
                            ]),
                            2
                        )
                    }));
                }
            break;
            case 'ice_wave':
                var targets = Game.getMobsInRange(this.position[0], this.position[1], a.range);
                if (targets.length) {
                    for (var i = 0; i < targets.length; i++) {
                        targets[i].addEffect(new IceEffect(a.interval, a.factor));
                    };

                    Game.effect_layer.add(new SpriteEffect({
                        lifetime: 10,
                        target: this,
                        graphics: new AnimatedSprite(
                            80,
                            80,
                            Game.sprite_pool.getList([
                                "img/blue_wave_0.png",
                                "img/blue_wave_1.png",
                                "img/blue_wave_2.png",
                                "img/blue_wave_3.png",
                                "img/blue_wave_4.png",
                            ]),
                            2
                        )
                    }));
                }
            break;
            case 'poison_wave':
                var targets = Game.getMobsInRange(this.position[0], this.position[1], a.range);
                if (targets.length) {
                    for (var i = 0; i < targets.length; i++) {
                        targets[i].addEffect(new PoisonEffect(a.interval, a.factor, a.damage));
                    };

                    Game.effect_layer.add(new SpriteEffect({
                        lifetime: 10,
                        target: this,
                        graphics: new AnimatedSprite(
                            80,
                            80,
                            Game.sprite_pool.getList([
                                "img/green_wave_0.png",
                                "img/green_wave_1.png",
                                "img/green_wave_2.png",
                                "img/green_wave_3.png",
                                "img/green_wave_4.png",
                            ]),
                            2
                        )
                    }));
                }
            break;
        }
        this.attack_interval_counter = a.interval;
    } else {
        this.attack_interval_counter--;
    }
}
