DirectDamageEffect = function(lifetime, dmg, delay) {
    this.lifetime = lifetime;
    this.dmg = dmg;
    this.delay = delay || 0;
}

DirectDamageEffect.prototype.apply = function(mob) {
    if (this.delay <= 0) {
        mob.hp -= this.dmg;
        this.lifetime--;
    } else {
        this.delay--;
    }
}

DirectDamageEffect.prototype.draw = function(mob) {
    if (this.delay <= 0) {
        var r = Math.floor(Math.random() * 3);
        Game.effect_layer.add(new SpriteEffect({
            lifetime: 4,
            target: mob,
            graphics: new StaticSprite(
                16,
                16,
                Game.sprite_pool.get("img/blood" + r + ".png")
            )
        }));
    }
}
