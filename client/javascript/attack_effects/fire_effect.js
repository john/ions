FireEffect = function(lifetime, dmg, delay) {
    this.lifetime = lifetime;
    this.dmg = dmg / lifetime;
    this.delay = delay || 0;
}

FireEffect.prototype.apply = function(mob) {
    if (this.delay <= 0) {
        mob.hp -= this.dmg;
        this.lifetime--;
    } else {
        this.delay--;
    }
}

FireEffect.prototype.draw = function(mob) {
    if (this.delay <= 0) {
        Game.effect_layer.add(new SpriteEffect({
            lifetime: 2,
            target: mob,
            graphics: new RandomizedSprite(
                16,
                16,
                Game.sprite_pool.getList([
                    "img/burn_0.png",
                    "img/burn_1.png",
                    "img/burn_2.png",
                ])
            )
        }));
    }
}
