PoisonEffect = function(lifetime, factor, dmg, delay) {
    this.lifetime = lifetime;
    this.factor = factor;
    this.dmg = dmg / lifetime;
    this.delay = delay || 0;
}

PoisonEffect.prototype.apply = function(mob) {
    if (this.delay <= 0) {
        mob.speed = mob.speed / this.factor;
        mob.hp -= this.dmg;
        this.lifetime--;
    } else {
        this.delay--;
    }
}

PoisonEffect.prototype.draw = function(mob) {
    if (this.delay <= 0) {
        Game.effect_layer.add(new SpriteEffect({
            lifetime: 2,
            target: mob,
            graphics: new RandomizedSprite(
                16,
                16,
                Game.sprite_pool.getList([
                    "img/poison_0.png",
                    "img/poison_1.png",
                    "img/poison_2.png",
                ])
            )
        }));
    }
}
