/**
 * @param {Game} parent
 */
SpritePool = function(parent, scale_down) {
    this.parent = parent;
    this.sprites = {};

    this.scale_down = scale_down;

    //internal counters
    this.to_load = 0;

    this.loaded = 0;
    this.errors = 0;

    // 1x1 pixel
    this.fallback_data = new Image();
    this.fallback_data.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
}

SpritePool.prototype.ready = function() {
    return this.to_load == this.loaded + this.errors;
};

/**
 * @param  {String} url
 * @return {Image}
 */
SpritePool.prototype.has = function(url) {
    if (this.sprites[url]) {
        return true;
    } else {
        return false;
    }
};

/**
 * @param  {String} url
 * @return {String}
 */
SpritePool.prototype.state = function(url) {
    return this.sprites[url].state || null;
};


/**
 * @param  {String} url
 * @return {Image}
 */
SpritePool.prototype.get = function(url) {
    if (this.has(url) && this.state(url) == 'ok') {
        return this.sprites[url].data;
    } else {
        Game.debugPrint('SpritePool: unknown sprite ' + url + ' requested');
        if (!this.has(url)) {
            this.registerSprite(url);
        }
        return this.fallback_data;
    }
};

/**
 * @param {String[]} urls
 */
SpritePool.prototype.getList = function(urls) {
    var l = [];
    for (var i = 0; i < urls.length; i++) {
        l.push(this.get(urls[i]));
    };
    return l;
};

/**
 * @param {String} url
 */
SpritePool.prototype.registerSprite = function(url) {
    if (this.has(url)) return;
    this.sprites[url] = {state: 'loading', data: null};
    this.to_load++;
    var self = this;
    var i = new Image();
    i.onload = function() {
        Game.debugPrint('SpritePool: sprite ' + url + ' loaded');

        var temp_canvas = document.createElement('canvas');
        temp_canvas.width = Math.floor(this.width / self.scale_down);
        temp_canvas.height = Math.floor(this.height / self.scale_down);

        var context = temp_canvas.getContext('2d');

        context.drawImage(this, 0, 0, this.width / self.scale_down, this.width / self.scale_down);

        var r = document.createElement("img");
        r.src = temp_canvas.toDataURL("image/png");

        self.sprites[url] = {state: 'ok', data: r};;
        self.loaded++;
    }
    i.onerror = function() {
        Game.debugPrint('SpritePool: sprite ' + url + ' could not be loaded');
        self.sprites[url] = {state: 'error', data: null};
        self.errors++;
    }
    i.src = url;
};

/**
 * @param {String} url
 * @param {Number} steps
 */
SpritePool.prototype.registerRotatingSprite = function(url, steps) {
    if (this.has(url)) return;
    this.sprites[url] = {state: 'loading', data: null};
    this.to_load++;
    var self = this;
    var img = new Image();
    img.onload = function() {

        Game.debugPrint('SpritePool: sprite ' + url + ' loaded');

        var w = Math.floor(this.width / self.scale_down);
        var h = Math.floor(this.height / self.scale_down);

        // canvas used for rotation
        var scale_canvas = document.createElement('canvas');
        scale_canvas.width = w;
        scale_canvas.height = h;
        var scale_context = scale_canvas.getContext('2d');
        scale_context.drawImage(this, 0, 0, w, h);

        // canvas used for rotation
        var rotation_canvas = document.createElement('canvas');
        rotation_canvas.width = w;
        rotation_canvas.height = h;
        var context = rotation_canvas.getContext('2d');

        // list of rotated sprites
        var arr = [];

        steps = Math.min(Game.settings.max_rotation_steps || 360, steps);

        // create rotated versions for each step and append to list
        for (var i = 0; i < steps; i++) {
            context.clearRect(0, 0, rotation_canvas.width, rotation_canvas.height);
            context.save();
            context.translate(w / 2, h / 2);
            context.rotate(360 / steps * i * Math.PI / 180);
            context.drawImage(scale_canvas, -w/2, -h/2);
            context.restore();
            var r = document.createElement("img");
            r.src = rotation_canvas.toDataURL("image/png");
            arr.push(r);
        };

        // add sprites to SpritePool
        self.sprites[url] = {state: 'ok', data: arr};
        self.loaded++;
    };

    img.onerror = function() {
        Game.debugPrint('SpritePool: sprite ' + url + ' could not be loaded');
        self.sprites[url] = {state: 'error', data: null};
        self.errors++;
    };

    img.src = url;
};
