MobList = function() {
    this.mobs = [];
    this.money = 0;
    this.mobs_survived = 0;
}

/**
 * @param {Mob} mob
 */
MobList.prototype.add = function(mob) {
    this.mobs.push(mob);
}

/**
 * @return {Number}
 */
MobList.prototype.getMoney = function() {
    var m = this.money;
    this.money = 0;
    return m;
}

/**
 * @return {Number}
 */
MobList.prototype.getSurvived = function() {
    var s = this.mobs_survived;
    this.mobs_survived = 0;
    return s;
}

/**
 * @param {Number} scale
 */
MobList.prototype.draw = function(scale) {
    for (var i = 0; i < this.mobs.length; i++) {
        this.mobs[i].draw(scale);
    }
}

MobList.prototype.update = function() {
    for (var i = 0; i < this.mobs.length; i++) {
        var mob = this.mobs[i];
        if (mob.pos_on_path >= Game.path.length) {
            mobSurvived(mob);
            this.mobs.splice(i, 1);
            i--;
            continue;
        } else if (mob.hp <= 0) {
            mobKilled(mob);
            this.mobs.splice(i, 1);
            i--;
            continue;
        } else {
            mob.update();
        }
    };
}

/**
 * @return {Mob}
 */
MobList.prototype.getNearestMob = function(x, y, max_dist) {
    var min = Infinity;
    var mob = false;
    for (var i = 0; i < this.mobs.length; i++) {
        var pos = this.mobs[i].getPos();
        var dist = distance(pos, [x, y]);
        if (dist < min && dist < max_dist) {
            min = dist;
            mob = this.mobs[i];
        }
    };
    return mob;
}

/**
 * @return {Mob[]}
 */
MobList.prototype.getMobsInRange = function(x, y, max_dist) {
    var list = [];
    for (var i = 0; i < this.mobs.length; i++) {
        var pos = this.mobs[i].getPos();
        var dist = distance(pos, [x, y]);
        if (dist < max_dist) {
            list.push(this.mobs[i]);
        }
    };
    return list;
}
