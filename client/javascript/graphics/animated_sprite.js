/**
 * @param {Number} width
 * @param {Number} height
 * @param {String[]} sprites
 * @param {Number} sprite_interval
 * @param {Boolean} reverse
 */
AnimatedSprite = function(width, height, sprites, sprite_interval, reverse) {
    this.width = width;
    this.height = height;
    this.sprites = sprites;
    this.sprite_interval = sprite_interval;
    this.nextSprite = 0;
    this.reverse = false;

    if (reverse) {
        this.draw = this.drawReverse;
    } else {
        this.draw = this.drawNoReverse;
    }
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
AnimatedSprite.prototype.drawReverse = function(x, y, scale) {
    var index = Math.floor(this.nextSprite / this.sprite_interval);

    if (index >= this.sprites.length - 1 || index < 0) {
        if (index > this.sprites.length - 1 || index < 0) {
            this.reverse = !this.reverse;
        }

        if (index >= this.sprites.length) {
            index = this.sprites.length -1;
        }
        if (index < 0) {
            index = 0;
        }
    }

    var w = Math.round(this.width * scale);
    var h = Math.round(this.height * scale);
    var x = Math.round(x * scale);
    var y = Math.round(y * scale);
    Game.ctx.drawImage(this.sprites[index], x - w / 2, y - h / 2, w, h);

    if (this.reverse) {
        if (index == 0) {
            this.nextSprite--;
        }
        this.nextSprite--;
    } else {
        if (index == this.sprites.length - 1) {
            this.nextSprite++;
        }
        this.nextSprite++;
    }
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
AnimatedSprite.prototype.drawNoReverse = function(x, y, scale) {
    var index = Math.ceil(this.nextSprite / this.sprite_interval);

    if (index > this.sprites.length - 1) {
            this.nextSprite = 0;
            index = 0;
    }

    var w = Math.round(this.width * scale);
    var h = Math.round(this.height * scale);
    var x = Math.round(x * scale);
    var y = Math.round(y * scale);
    Game.ctx.drawImage(this.sprites[index], x - w / 2, y - h / 2, w, h);

    this.nextSprite++;
}
