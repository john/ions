/**
 * @param {Number} width
 * @param {Number} height
 * @param {String} sprite
 */
StaticSprite = function(width, height, sprite) {
    this.width = width;
    this.height = height;
    this.sprite = sprite;
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
StaticSprite.prototype.draw = function(x, y, scale) {
    var w = Math.round(this.width * scale);
    var h = Math.round(this.height * scale);
    var x = Math.round(x * scale);
    var y = Math.round(y * scale);
    Game.ctx.drawImage(this.sprite, x - w / 2, y - h / 2, w, h);
}
