/**
 * @param {Number} width
 * @param {Number} height
 * @param {String} sprite
 */
RotatingSprite = function(width, height, sprites) {
    this.width = width;
    this.height = height;
    this.sprites = sprites;
    this.steps = this.sprites.length;
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 * @param  {Number} deg
 */
RotatingSprite.prototype.draw = function(x, y, scale, deg) {
    var w = Math.round(this.width * scale);
    var h = Math.round(this.height * scale);
    var x = Math.round(x * scale);
    var y = Math.round(y * scale);
    var index = Math.floor(deg / 360 * this.steps);
    Game.ctx.drawImage(this.sprites[index] || this.sprites[0], x - w / 2, y - h / 2, w, h);
}
