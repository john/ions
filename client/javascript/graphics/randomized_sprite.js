/**
 * @param {Number} width
 * @param {Number} height
 * @param {String} sprite
 */
RandomizedSprite = function(width, height, sprites) {
    this.width = width;
    this.height = height;
    this.sprites = sprites;
}

/**
 * @param  {Number} x
 * @param  {Number} y
 * @param  {Number} scale
 */
RandomizedSprite.prototype.draw = function(x, y, scale) {
    var index = Math.floor(Math.random() * this.sprites.length);

    var w = Math.round(this.width * scale);
    var h = Math.round(this.height * scale);
    var x = Math.round(x * scale);
    var y = Math.round(y * scale);
    Game.ctx.drawImage(this.sprites[index], x - w / 2, y - h / 2, w, h);
}
