/**
 * @param {Number} num
 * @return {Number}
 */
toInternalPosition = function(num) {
    return num * 36 + 18;
}

/**
 * @param {Vector2} v1
 * @param {Vector2} v2
 * @return {Vector2}
 */
distance = function(v1, v2) {
    return Math.sqrt(Math.pow(Math.abs(v1[0] - v2[0]), 2) + Math.pow(Math.abs(v1[1] - v2[1]), 2));
}

DummyTarget = function(pos) {
    this.pos = pos;
}

DummyTarget.prototype.getPos = function() {
    return this.pos;
}

