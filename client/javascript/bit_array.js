/**
 * @param {Number} length
 */
BitArray = function(length) {
    this.length = length;
    this.real_lenth = Math.ceil(length / 32);
    this.data = new Uint32Array(this.real_lenth);
}

/**
 * @param {Number} position
 * @param {Number} val
 */
BitArray.prototype.setAt = function(position, val) {
    var pos = Math.floor(position / 32);
    var shift =  31 - (position % 32);
    if (val) {
        this.data[pos] = this.data[pos] | (1 << shift);
    } else {
        this.data[pos] = this.data[pos] & ~(1 << shift);
    }
}

/**
 * @param {Number} position
 */
BitArray.prototype.getAt = function(position) {
    var pos = Math.floor(position / 32);
    var shift =  31 - (position % 32);
    return ((this.data[pos] & (1 << shift)) == (1 << shift));
}
