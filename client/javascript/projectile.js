/**
 * @param {Mob} def
 */
Projectile = function(from, target, speed, graphics) {
    this.pos_on_path = 0;
    this.speed = speed;
    this.graphics = graphics;
    this.path = [];

    var to = target.getPos();
    var dist = distance([from[0], from[1]], [to[0], to[1]]);
    var segments = Math.floor(dist / speed);
    to = target.getFuturePos(segments);
    var segment_size = Math.floor(dist / segments);

    for (var i = 0; i < segments; i++) {
        this.path.push(this.pointInPath(from[0], to[0], from[1], to[1], dist, segment_size, i));
    };
}

/**
 * @param {Number} scale
 */
Projectile.prototype.draw = function(scale) {
    var pos = this.getPos();
    this.graphics.draw(pos[0], pos[1], scale);
}

/**
 * @return {Vector2}
 */
Projectile.prototype.getPos = function() {
    return this.path[this.pos_on_path] || [0, 0];
}

Projectile.prototype.update = function() {
    this.pos_on_path++;
}

/**
 * @param {Number} x1
 * @param {Number} x2
 * @param {Number} y1
 * @param {Number} y2
 * @param {Number} distance
 * @param {Number} segment_size
 * @param {Number} segment
 * @return {Vector2}
 */
Projectile.prototype.pointInPath = function(x1, x2, y1, y2, distance, segment_size, segment) {
    var r = segment * segment_size / distance;
    var x3, y3;

    y1 = -y1;
    y2 = -y2;

    x3 = r * x2 + (1 - r) * x1;
    y3 = r * y2 + (1 - r) * y1;
    return [x3, -y3];
}
