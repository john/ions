SinglePlayer = function() {

    this.wave = -1;

    this.waves = [
        {
            mob_type: 'enemy1',
            spawn_interval: 40,
            count: 5
        },
        {
            mob_type: 'enemy1',
            spawn_interval: 30,
            count: 10
        },
        {
            mob_type: 'enemy1',
            spawn_interval: 20,
            count: 15
        },
        {
            mob_type: 'enemy1',
            spawn_interval: 10,
            count: 20
        },
        {
            mob_type: 'enemy2',
            spawn_interval: 30,
            count: 10
        },
        {
            mob_type: 'enemy2',
            spawn_interval: 20,
            count: 10
        },
        {
            mob_type: 'enemy2',
            spawn_interval: 15,
            count: 10
        },
        {
            mob_type: 'enemy2',
            spawn_interval: 10,
            count: 10
        },
        {
            mob_type: 'enemy3',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy3',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy3',
            spawn_interval: 10,
            count: 10
        },
        {
            mob_type: 'enemy3',
            spawn_interval: 10,
            count: 20
        },
        {
            mob_type: 'enemy4',
            spawn_interval: 20,
            count: 15
        },
        {
            mob_type: 'enemy4',
            spawn_interval: 15,
            count: 15
        },
        {
            mob_type: 'enemy4',
            spawn_interval: 10,
            count: 15
        },
        {
            mob_type: 'enemy4',
            spawn_interval: 8,
            count: 10
        },
        {
            mob_type: 'enemy5',
            spawn_interval: 30,
            count: 15
        },
        {
            mob_type: 'enemy5',
            spawn_interval: 15,
            count: 15
        },
        {
            mob_type: 'enemy5',
            spawn_interval: 12,
            count: 15
        },
        {
            mob_type: 'enemy5',
            spawn_interval: 10,
            count: 15
        },
        {
            mob_type: 'enemy6',
            spawn_interval: 30,
            count: 20
        },
        {
            mob_type: 'enemy6',
            spawn_interval: 20,
            count: 10
        },
        {
            mob_type: 'enemy6',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy6',
            spawn_interval: 10,
            count: 15
        },
        {
            mob_type: 'enemy7',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy7',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy7',
            spawn_interval: 11,
            count: 15
        },
        {
            mob_type: 'enemy7',
            spawn_interval: 9,
            count: 15
        },
        {
            mob_type: 'enemy8',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy8',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy8',
            spawn_interval: 12,
            count: 20
        },
        {
            mob_type: 'enemy8',
            spawn_interval: 10,
            count: 20
        },
        {
            mob_type: 'enemy9',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy9',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy9',
            spawn_interval: 12,
            count: 15
        },
        {
            mob_type: 'enemy9',
            spawn_interval: 10,
            count: 15
        },
        {
            mob_type: 'enemy10',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy10',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy10',
            spawn_interval: 12,
            count: 15
        },
        {
            mob_type: 'enemy10',
            spawn_interval: 9,
            count: 15
        },
        {
            mob_type: 'enemy11',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy11',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy11',
            spawn_interval: 12,
            count: 15
        },
        {
            mob_type: 'enemy11',
            spawn_interval: 9,
            count: 15
        },
        {
            mob_type: 'enemy12',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy12',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy12',
            spawn_interval: 12,
            count: 15
        },
        {
            mob_type: 'enemy12',
            spawn_interval: 9,
            count: 15
        },
        {
            mob_type: 'enemy13',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy13',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy13',
            spawn_interval: 12,
            count: 20
        },
        {
            mob_type: 'enemy13',
            spawn_interval: 9,
            count: 20
        },
        {
            mob_type: 'enemy14',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy14',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy14',
            spawn_interval: 12,
            count: 20
        },
        {
            mob_type: 'enemy14',
            spawn_interval: 9,
            count: 20
        },
        {
            mob_type: 'enemy15',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy15',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy15',
            spawn_interval: 12,
            count: 20
        },
        {
            mob_type: 'enemy15',
            spawn_interval: 9,
            count: 20
        },
        {
            mob_type: 'enemy16',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy16',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy16',
            spawn_interval: 10,
            count: 20
        },
        {
            mob_type: 'enemy16',
            spawn_interval: 8,
            count: 20
        },
        {
            mob_type: 'enemy17',
            spawn_interval: 20,
            count: 20
        },
        {
            mob_type: 'enemy17',
            spawn_interval: 15,
            count: 20
        },
        {
            mob_type: 'enemy17',
            spawn_interval: 12,
            count: 20
        },
        {
            mob_type: 'enemy17',
            spawn_interval: 9,
            count: 20
        }
    ]

}

SinglePlayer.prototype.nextWave = function() {
    this.wave++;
    if (this.waves[this.wave]) {
        Session.set('wave', this.wave);
        var w = this.waves[this.wave];
        Game.spawn_interval = w.spawn_interval;
        Game.spawnMob(w.mob_type, w.count);
    } else {
        alert('you won!!!!11');
    }
}
