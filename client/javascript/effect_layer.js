EffectLayer = function() {
    this.effects = [];
}

/**
 * @param {Effect} effect
 */
EffectLayer.prototype.add = function(effect) {
    this.effects.push(effect);
}

/**
 * @param {Number} scale
 */
EffectLayer.prototype.draw = function(scale) {
    for (var i = 0; i < this.effects.length; i++) {
        var e = this.effects[i];
        if (e.lifetime != 0) {
            e.draw(scale, Game.ctx);
            if (e.lifetime > 0) {
                e.lifetime--;
            }
        } else {
            this.effects.splice(i, 1);
            i--;
        }
    };
}
