ProjectileList = function() {
    this.projectiles = [];
}

/**
 * @param {Projectile} projectile
 */
ProjectileList.prototype.add = function(projectile) {
    this.projectiles.push(projectile);
}

/**
 * @param {Number} scale
 */
ProjectileList.prototype.draw = function(scale) {
    for (var i = 0; i < this.projectiles.length; i++) {
        this.projectiles[i].draw(scale);
    }
}

ProjectileList.prototype.update = function() {
    for (var i = 0; i < this.projectiles.length; i++) {
        if (this.projectiles[i].pos_on_path >= this.projectiles[i].path.length) {
            this.projectiles.splice(i, 1);
            i--;
            continue;
        } else {
            this.projectiles[i].update();
        }
    };
}
