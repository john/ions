if (typeof(Engine) == "undefined") {
    Engine = {};
}

Engine.Game = function (map_layer, drawing_layer, single_player) {
    if (typeof Game != "undefined") return;
    Game = this;

    this.settings = {
        debug: localStorage.getItem('GAME_DEBUG') || true,
        max_width: localStorage.getItem('GAME_MAX_WIDTH') || 1300,
        max_rotation_steps: localStorage.getItem('GAME_MAX_ROTATION_STEPS') || 360
    };

    this.simulation_speed = 1;
    this.spawn_interval = 20;
    this.scale = 1;

    this.spawn_timer = 0;
    this.delta = 0;
    this.clock = 0;

    this.last_frame_time_ms = false;

    this.selected = false;

    this.map_layer = map_layer;
    this.drawing_layer = drawing_layer;

    this.ctx = this.drawing_layer.getContext("2d");
    this.effect_layer = new EffectLayer(drawing_layer);

    this.spawn_queue = [];
    this.towers = [];
    this.grid = [];
    this.mob_list = new MobList();
    this.projectile_list = new ProjectileList();

    var scale_down = 192 / Math.floor(window.innerWidth / 10);
    this.sprite_pool = new SpritePool(this, scale_down);

    this.loadSprites();

    this.waitForSpritePool = window.setInterval(function () {
        if (Game.sprite_pool.ready()) {
            Game.initMap();
            window.clearInterval(Game.waitForSpritePool);
        }
    }, 100);

    if (single_player) {
        this.single_player = new SinglePlayer();
    }
}

Engine.Game.prototype.restart = function () {
    Session.set("loading", true);
    resetPlayer();

    this.single_player = new SinglePlayer();

    this.ctx.clearRect(0, 0, this.drawing_layer.width, this.drawing_layer.height);
    this.selected = false;
    this.spawn_queue = [];
    this.towers = [];
    this.grid = [];
    this.mob_list = new MobList();
    this.projectile_list = new ProjectileList();
    this.effect_layer = new EffectLayer(this.drawing_layer);
    this.initMap()
}


Engine.Game.prototype.debugPrint = function (msg) {
    if (this.settings.debug) {
        console.debug(msg);
    }
}


Engine.Game.prototype.loadSprites = function () {
    loadTowerSprites();
    loadMobSprites();

    this.sprite_pool.registerSprite("img/noise.png");
    this.sprite_pool.registerSprite("img/block.png");
    this.sprite_pool.registerRotatingSprite("img/spawn.png", 180);
    this.sprite_pool.registerRotatingSprite("img/end.png", 180);
}

Engine.Game.prototype.initMap = function () {
    this.map = new Map(this.map_layer, this.sprite_pool);

    var x = MAP;
    this.map.fill(x);
    this.map.draw(this.scale);

    this.calculatePath();

    Game.effect_layer.add(new SpriteEffect({
        lifetime: -1,
        target: new DummyTarget(this.map.spawn_point),
        graphics: new AnimatedSprite(
            36,
            36,
            this.sprite_pool.get('img/spawn.png'),
            1,
            false
        )
    }));

    Game.effect_layer.add(new SpriteEffect({
        lifetime: -1,
        target: new DummyTarget(this.map.end_point),
        graphics: new AnimatedSprite(
            36,
            36,
            this.sprite_pool.get('img/end.png'),
            1,
            false
        )
    }));

    for (var i = 0; i < 20; i++) {
        this.grid[i] = [];
        for (var j = 0; j < 10; j++) {
            this.grid[i][j] = false;
        }
    }

    this.drawing_layer.onclick = function (ev) {
        Game.handleClick(ev.pageX, ev.pageY - this.offsetTop);
    }

    this.setSize(window.innerWidth);
    window.onresize = function () {
        Game.setSize(window.innerWidth);
    }

    Session.set("loading", false);

    if (!this.initialized) {
        window.requestAnimationFrame(update);
    }
    this.initialized = true;
}

/**
 * @param {Number} x
 * @param {Number} y
 */
Engine.Game.prototype.handleClick = function (x, y) {
    // translate position on canvas to grid position
    x = Math.floor(10 / this.drawing_layer.width * x);
    y = Math.floor(20 / this.drawing_layer.height * y);

    this.debugPrint('click at: x ' + x + ' y ' + y);

    // store click position
    this.selected = [x, y];

    if (this.grid[y][x] == false && this.map.build_grid[y][x]) { // block is build area and free
        this.debugPrint('opening towerBuyMenu');
        Session.set("towerUpgradeMenuVisible", false);
        Session.set("towerBuyMenuVisible", true);
        var tower = this.grid[y][x];
        Session.set("selectedTower", {
            level: tower.level,
            def: tower.def
        });
    } else if (this.grid[y][x] != false && this.map.build_grid[y][x]) { // block is build area and not free
        this.debugPrint('opening towerUpgradeMenu');
        var tower = this.grid[y][x];
        Session.set("selectedTower", {
            level: tower.level,
            def: tower.def
        });
        Session.set("towerBuyMenuVisible", false);
        Session.set("towerUpgradeMenuVisible", true);
    } else {
        Session.set("selectedTower", '');
        Session.set("towerUpgradeMenuVisible", false);
        Session.set("towerBuyMenuVisible", false)
    }
}

/**
 * @param {Number} width
 */
Engine.Game.prototype.setSize = function (width) {

    width = Math.min(width, this.settings.max_width);

    if (width >= 1300) {
        width = 1000; // desktop mode
    }

    this.debugPrint('setting Game width to ' + width + ' px');
    this.scale = width / 360;
    this.drawing_layer.width = width;
    this.drawing_layer.height = width * 2;
    this.map_layer.width = width;
    this.map_layer.height = width * 2;
    this.map.draw(this.scale);
}

/**
 * @param {Number} speed
 */
Engine.Game.prototype.setSpeed = function (speed) {
    this.debugPrint('setting Game speed to ' + speed);
    this.simulation_speed = speed;
}

Engine.Game.prototype.getWave = function () {
    return this.single_player.wave;
}

Engine.Game.prototype.nextWave = function () {
    return this.single_player.nextWave();
}

Engine.Game.prototype.calculatePath = function () {
    this.debugPrint('calculating path');
    this.path = findPath(this.map.obstacle_map, 720, 720, this.map.spawn_point, this.map.end_point);
    this.debugPrint('path length ' + this.path.length);
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Number} max_dist
 * @return {Mob}
 */
Engine.Game.prototype.getNearestMob = function (x, y, max_dist) {
    return this.mob_list.getNearestMob(x, y, max_dist);
}

/**
 * @param {Number} x
 * @param {Number} y
 * @param {Number} max_dist
 * @return {Mob[]}
 */
Engine.Game.prototype.getMobsInRange = function (x, y, max_dist) {
    return this.mob_list.getMobsInRange(x, y, max_dist);
}

Engine.Game.prototype.benchmark = function () {
    var def = getTower('sniper_tower');
    for (var i = 0; i < 20; i++) {
        this.grid[i] = [];
        for (var j = 0; j < 10; j++) {
            var t = new Tower(toInternalPosition(j), toInternalPosition(i), def);
            this.towers.push(t);
            this.grid[i][j] = t;
        }
    }
}

/**
 * @param {Projectile} projectile
 */
Engine.Game.prototype.spawnProjectile = function (projectile) {
    this.projectile_list.add(projectile);
}

/**
 * @param {String} mob_type
 */
Engine.Game.prototype.spawnMob = function (mob_type, count) {
    sendMobToEnemy(mob_type, count || 10);
}

Engine.Game.prototype.getSelectedTower = function () {
    return this.grid[this.selected[1]][this.selected[0]];
}

Engine.Game.prototype.upgradeTower = function () {
    var tower = this.getSelectedTower();
    if (tower.canUpgrade()) {
        tower.upgrade();
    }
}

/**
 * @param {Tower} tower
 */
Engine.Game.prototype.spawnTower = function (tower) {
    if (this.selected) {
        var x = this.selected[0];
        var y = this.selected[1];
        if (!this.grid[y][x] && this.map.build_grid[y][x] && $('#playerMoney').attr('data-money') >= tower.levels[0].price) {
            var def = tower;
            var x = this.selected[0];
            var y = this.selected[1];
            var t = new Tower(toInternalPosition(x), toInternalPosition(y), def);
            this.towers.push(t);
            this.grid[y][x] = t;
            buildTower(tower);
        }
    }
}

Engine.Game.prototype.sellTower = function () {
    var x = this.selected[0];
    var y = this.selected[1];
    var tower = this.grid[y][x];
    var i = this.towers.indexOf(tower);
    this.towers.splice(i, 1);
    this.grid[y][x] = false;
}


Engine.Game.prototype.update = function (timestamp) {

    var timestep = 1000 / 60;
    this.delta += timestamp - this.last_frame_time_ms || timestamp;
    this.last_frame_time_ms = timestamp;

    if (this.delta > timestep * 5) {
        this.delta = timestep * 5; //limit delta to prevent glitches
    }

    while (this.delta >= timestep) {
        for (var i = 0; i < this.simulation_speed; i++) {

            var wave = popMobFromQueue();
            if (wave) {
                var def = getMobByType(wave.mob_type);
                for (var j = 0; j < wave.count; j++) {
                    this.spawn_queue.push(new Mob(def));
                }
            }

            if (this.spawn_queue.length > 0) {
                if (this.spawn_timer == 0) {
                    this.mob_list.add(this.spawn_queue.shift());
                    this.spawn_timer = this.spawn_interval;
                } else {
                    this.spawn_timer--;
                }
            }

            for (var j = 0; j < this.towers.length; j++) {
                this.towers[j].attack();
            }
            this.projectile_list.update();
            this.mob_list.update();
        }
        this.delta -= timestep;
    }

    if (this.single_player && getPlayer().hp <= 1) {
        alert('You loose');
        this.restart();
    }

    this.ctx.clearRect(0, 0, this.drawing_layer.width, this.drawing_layer.height);
    var scale = this.scale;
    //this.ctx.shadowColor = 'none';
    this.projectile_list.draw(scale);
    this.mob_list.draw(scale);
    this.effect_layer.draw(scale);

    for (var i = 0; i < this.towers.length; i++) {
        var tower = this.towers[i];
        var pos = tower.position;
        if (tower.rotation) {
            tower.graphics.draw(pos[0], pos[1], scale, tower.rotation);
        } else {
            tower.graphics.draw(pos[0], pos[1], scale);
        }
    }

    if (this.selected) {
        var tower = this.grid[this.selected[1]][this.selected[0]];
        var centerX = toInternalPosition(this.selected[0]) * scale;
        var centerY = toInternalPosition(this.selected[1]) * scale;

        if (tower) {
            var radius = tower.def.levels[tower.level].attack.range * scale;
        } else if (this.map.build_grid[this.selected[1]][this.selected[0]]) {
            var radius = 15 * scale;
        }

        this.ctx.beginPath();
        this.ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        this.ctx.lineWidth = 2;
        this.ctx.strokeStyle = 'green';
        this.ctx.stroke();
    }
}


update = function (timestamp) {
    Game.update(timestamp);
    if (!window.stop_game) {
        window.requestAnimationFrame(update);
    }
}
