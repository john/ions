Template.towerBuyMenu.helpers({
    active: function () {
        return Session.get("towerBuyMenuVisible") ? "active" : "";
    },
    visible: function () {
        return Session.get("towerBuyMenuVisible");
    },

    towers: function () {
        var ret = [];
        var towers = Towers.find({}).fetch();

        towers.forEach(function (tower) {
            var level = tower.levels[0];

            ret.push({
                type: tower.type,
                displayName: level.displayName,
                cost: level.price,
                enoughMoney: level.price > getMoney() ? "red" : "",
                img: level.sprite || level.sprites[0] || "",
                range: level.attack.range,
                damage: level.attack.damage,
                speed: level.attack.interval,
                splash: level.attack.splash,
                slow: level.attack.factor,

            });
        });

        return ret;
    }
});

Template.towerBuyMenu.events({
    'click #tower_buy_menu .close': function (event, template) {
        event.preventDefault();

        Game.selected = false;
        Session.set("towerBuyMenuVisible", false);
    },

    'click #tower_buy_menu .buy': function (event, template) {
        event.preventDefault();
        var tower = getTower($(event.target).attr('data-towername'));
        Game.spawnTower(tower);
        Game.selected = false;
    }
});
