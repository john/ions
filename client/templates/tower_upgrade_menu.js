Template.towerUpgradeMenu.helpers({
    active: function () {
        return Session.get("towerUpgradeMenuVisible") ? "active" : "";
    },
    visible: function () {
        return Session.get("towerUpgradeMenuVisible");
    },
    tower: function () {
        return Session.get("selectedTower");
    },
    displayName: function (tower) {
        return tower.def.levels[tower.level].displayName;
    },
    upgradeAvailable: function (tower) {
        return (tower.def.levels[tower.level + 1]) != undefined;
    },
    upgradeCost: function (tower) {
        return tower.def.levels[tower.level + 1].price;
    },
    enoughMoney: function (tower) {
        return (tower.def.levels[tower.level + 1].price > getMoney()) ? "red" : "";
    },
    sellValue: function (tower) {
        return calcSellPrice(tower.def, tower.level);
    }
});

Template.towerUpgradeMenu.events({
    'click #tower_menu .close': function (event, template) {
        event.preventDefault();

        Game.selected = false;
        Session.set("towerUpgradeMenuVisible", false);
    },

    'click #tower_menu .upgrade': function (event, template) {
        event.preventDefault();

        var tower = Game.getSelectedTower();

        if (tower) {
            upgradeTower(tower);
        } else {
            Session.set("towerUpgradeMenuVisible", false);
        }
    },

    'click #tower_menu .sell': function (event, template) {
        event.preventDefault();

        var tower = Game.getSelectedTower();

        if (tower) {
            sellTower(tower);
        } else {
            Session.set("towerUpgradeMenuVisible", false);
        }
    }
});
