if (Meteor.isClient) {
    Template.matchFinished.helpers({
        winner_looser: function () {
            var winner = getMatch().winner == getPlayerId();
            return (winner) ? "WINNER" : "LOOSER";
        }
    });

    Template.matchFinished.events({
        'click .restart': function (event, template) {
            event.preventDefault();

            restartGame(getPlayerId());

            return false;
        }
    });
}