if (Meteor.isClient) {

    Template.login.helpers({
        errorMessage: function () {
            return Session.get("errorMessage");
        }
    })
    Template.login.events({
        'submit .start': function (event, template) {
            event.preventDefault();

            if (template.$('#email').val() == '' || template.$('#password').val() == '') {
                return false;
            }

            var email = template.$('#email').val();
            var password = template.$('#password').val().hashCode();

            Meteor.call("insertOrLoginPlayer", email, password, function (error, result) {
                if (result) {
                    Session.set("player", result);
                    subscribeAll();
                    Session.set("loggedIn", result != undefined);
                    Session.set("errorMessage", "");
                } else {
                    Session.set("errorMessage", "password wrong");
                }

            });

            return false;
        },
        'submit .singleplayer': function (event, template) {
            event.preventDefault();
            if (template.$('#email').val() == '' || template.$('#password').val() == '') {
                return false;
            }

            $('body').addClass('sp');

            var email = template.$('#email').val();
            var password = template.$('#password').val().hashCode();
            Meteor.call("insertOrLoginPlayer", email, password, function (error, result) {
                Session.set("player", result);

                subscribeAll();

                Session.set("singleplayer", true);
                Session.set("loggedIn", result != undefined);
            });
            return false;
        }
    });
}
