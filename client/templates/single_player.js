Template.singlePlayer.events({
    'click .next-wave': function (event, template) {
        Game.nextWave();
        return false;
    },
    'click .pause': function (event, template) {
        Game.setSpeed(0);
    },
    'click .play': function (event, template) {
        Game.setSpeed(1);
    },
    'click .fast': function (event, template) {
        Game.setSpeed(2);
    },
    'click .faster': function (event, template) {
        Game.setSpeed(4);
    },
});
