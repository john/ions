Template.map.rendered = function () {
    if (!this._rendered) {
        this._rendered = true;
        Game = new Engine.Game(document.getElementById("background_canvas"), document.getElementById("walker_canvas"), Session.get("singleplayer"));
    }
}

Template.map.gestures({
    'swipeleft .swipe': function () {
        if (!Session.get("spawnMenuVisible")) {
            Session.set("spawnMenuVisible", true);
        }
    }
});
