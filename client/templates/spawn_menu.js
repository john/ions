Template.spawnMenu.helpers({
    active: function () {
        return Session.get("spawnMenuVisible") ? "active" : "";
    },
    mobs: function () {
        var ret = [];
        var mobs = Mobs.find({}).fetch();

        mobs.forEach(function (mob) {
            ret.push({
                type: mob.type,
                img: mob.sprite || mob.sprites[0] || "",
                enoughMoney: function (count) {
                    return (count * mob.price) > getMoney() ? "red" : ""
                }
            });
        });

        return ret;
    }
});

Template.spawnMenu.events({
    'click .open': function (event, template) {
        event.preventDefault();

        Session.set("spawnMenuVisible", true);
    },
    'click .close': function (event, template) {
        event.preventDefault();

        Session.set("spawnMenuVisible", false);
    },
    'click li.spawn': function (event, template) {
        event.preventDefault();
        Game.spawnMob($(event.target).attr('data-type'), $(event.target).attr('data-count'));
    }
});

Template.spawnMenu.gestures({
    'swiperight .swipe': function () {
        if (Session.get("spawnMenuVisible")) {
            Session.set("spawnMenuVisible", false);
        }
    }
})
