if (Meteor.isClient) {
    Template.statsBar.helpers({
        hp: function () {
            return getHP();
        },
        money: function () {
            return getMoney();
        },
        enemyHp: function () {
            return getEnemyHP();
        },
        enemyMoney: function () {
            return getEnemyMoney();
        },
        isSingleplayer: function () {
            return Session.get('singleplayer');
        },
        wave: function () {
            return Session.get('wave');
        }
    });
}
