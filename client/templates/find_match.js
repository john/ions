if (Meteor.isClient) {

    Template.findMatch.helpers({
        matches: function () {
            return Matches.find({
                player2: ""
            });
        },
        playersOnline: function () {
            return Players.find({online: true}).count();
        }
    });

    Template.match.helpers({
        playerStats: function (player_id) {
            return getPlayerById(player_id).email + " (" + getPlayerById(player_id).wins + "|" + getPlayerById(player_id).looses + ")";
        }
    });

    Template.findMatch.events({
        'click .joinMatch': function (event, template) {
            event.preventDefault();
            console.log("join");

            var match_id = $(event.target).attr('data-value');

            joinMatch(match_id);

            return false;
        },
        'click .createMatch': function (event, template) {
            event.preventDefault();
            console.log("create");

            createMatch();

            return false;
        }
    });
}