# Coding Guidelines
## Naming of
### Classes:
    TestClass (upper camelcase)

### Functions/Methods:
    someAwesomeFunction (lower camelcase)

### Parameters/Variables/Files:
    sweet_snake_case (snakecase)

## Brackets

    if (...) {
        do_onliner_stuff;
    } else {

    }

---

    do {

    } while (...)

---

    for (...) {

    }

---

    while (...) {

    }

- insert a single space
    - after the `if`, `while`, `for`, `do`
    - inbetween of `)` and `{`
    - inbetween of `else` and `{`
- always have a line break after an opening `{`
- if there is need for an `else` block have it on the same line as the `}`
