Meteor.publish("players", function () {
    return Players.find({}, {
        fields: {
            "password": 0
        }
    });
});

Meteor.publish("towers", function () {
    return Towers.find({});
});

Meteor.publish("mobs", function () {
    return Mobs.find({});
});

Meteor.publish("mob_queue", function (player_id) {
    return MobQueue.find({
        player_id: player_id
    });
});

Meteor.publish("matches", function (player_id) {
    this._session.socket.on("close",
        Meteor.bindEnvironment(function (err, res) {
            playerDisconnected(player_id);
        })
    );
    return Matches.find();
});
