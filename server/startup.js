if (Meteor.isServer) {
    Meteor.startup(function () {
        Matches.remove({});
        MobQueue.remove({});
        Players.update(
            {},
            {
                $set: {
                    online: false,
                    inMatch: false
                }
            }, {
                multi: true
            }
        );
    });
}