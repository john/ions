if (Meteor.isServer) {
    Meteor.startup(function () {

        Towers.remove({});

        Towers.upsert({
            type: 'laser_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 600,
                        displayName: "weak laser tower",
                        rotates: true,
                        sprites: [
                            'img/lasertower_0.png'
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'laser',
                            color: "rgba(240, 40, 80, 0.5)",
                            damage: 8,
                            range: 60,
                            interval: 16
                        }
                    },
                    {
                        level: 1,
                        price: 800,
                        displayName: "laser tower",
                        rotates: true,
                        sprites: [
                            'img/lasertower_1.png'
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'laser',
                            color: "rgba(40, 240, 80, 0.5)",
                            damage: 16,
                            range: 80,
                            interval: 10
                        }
                    },
                    {
                        level: 2,
                        price: 1000,
                        displayName: "strong laser tower",
                        rotates: true,
                        sprites: [
                            'img/lasertower_2.png'
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'laser',
                            color: "rgba(40, 80, 240, 0.5)",
                            damage: 32,
                            range: 100,
                            interval: 10
                        }
                    }
                ]
            }
        });

        Towers.upsert({
            type: 'gun_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 500,
                        displayName: "weak gun tower",
                        rotates: true,
                        sprites: [
                            "img/guntower_0.png"
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'bullets',
                            bullet_speed: 5,
                            damage: 2,
                            range: 80,
                            interval: 10
                        }
                    },
                    {
                        level: 1,
                        price: 800,
                        displayName: "gun tower",
                        rotates: true,
                        sprites: [
                            "img/guntower_1.png"
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'bullets',
                            bullet_speed: 5,
                            damage: 4,
                            range: 100,
                            interval: 7
                        }
                    },
                    {
                        level: 2,
                        price: 1000,
                        displayName: "strong gun tower",
                        rotates: true,
                        sprites: [
                            "img/guntower_2.png"
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'bullets',
                            bullet_speed: 5,
                            damage: 6,
                            range: 110,
                            interval: 4
                        }
                    }
                ]
            }
        });

        Towers.upsert({
            type: 'bomb_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 500,
                        displayName: "weak bomb tower",
                        rotates: true,
                        sprites: [
                            "img/guntower_0.png"
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'bomb',
                            bullet_speed: 4,
                            splash: 20,
                            damage: 6,
                            range: 80,
                            interval: 30
                        }
                    },
                    {
                        level: 0,
                        price: 500,
                        displayName: "bomb tower",
                        rotates: true,
                        sprites: [
                            "img/guntower_0.png"
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'bomb',
                            bullet_speed: 4,
                            splash: 30,
                            damage: 10,
                            range: 80,
                            interval: 27
                        }
                    },
                    {
                        level: 0,
                        price: 500,
                        displayName: "strong bomb tower",
                        rotates: true,
                        sprites: [
                            "img/guntower_0.png"
                        ],
                        rotation_steps: 90,
                        attack: {
                            type: 'bomb',
                            bullet_speed: 4,
                            splash: 40,
                            damage: 12,
                            range: 80,
                            interval: 25
                        }
                    }
                ]
            }
        });

        Towers.upsert({
            type: 'sniper_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 900,
                        displayName: "weak sniper tower",
                        rotates: true,
                        rotation_steps: 90,
                        sprites: [
                            "img/snipertower_0.png"
                        ],
                        attack: {
                            type: 'bullets',
                            bullet_speed: 6,
                            damage: 20,
                            range: 500,
                            interval: 80
                        }
                    },
                    {
                        level: 1,
                        price: 1400,
                        displayName: "sniper tower",
                        rotates: true,
                        rotation_steps: 90,
                        sprites: [
                            "img/snipertower_1.png"
                        ],
                        attack: {
                            type: 'bullets',
                            bullet_speed: 6,
                            damage: 40,
                            range: 500,
                            interval: 80
                        }
                    },
                    {
                        level: 1,
                        price: 1400,
                        displayName: "sniper tower",
                        rotates: true,
                        rotation_steps: 90,
                        sprites: [
                            "img/snipertower_2.png"
                        ],
                        attack: {
                            type: 'bullets',
                            bullet_speed: 8,
                            damage: 40,
                            range: 500,
                            interval: 40
                        }
                    }
                ]
            }
        });

        Towers.upsert({
            type: 'fire_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 1000,
                        displayName: "fire wave tower",
                        sprites: [
                            "img/fire_tower_0.png"
                        ],
                        spriteInterval: 1,
                        attack: {
                            type: 'fire_wave',
                            damage: 3,
                            range: 45,
                            interval: 30
                        }
                    },
                    {
                        level: 1,
                        price: 1200,
                        displayName: "strong fire wave tower",
                        sprites: [
                            "img/fire_tower_1.png"
                        ],
                        spriteInterval: 1,
                        attack: {
                            type: 'fire_wave',
                            damage: 6,
                            range: 50,
                            interval: 30
                        }
                    }
                ]
            }
        });

        Towers.upsert({
            type: 'poison_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 1200,
                        displayName: "poison wave tower",
                        sprites: [
                            "img/poison_tower_0.png"
                        ],
                        spriteInterval: 6,
                        attack: {
                            type: 'poison_wave',
                            damage: 4,
                            factor: 1.5,
                            range: 45,
                            interval: 70
                        }
                    },
                    {
                        level: 0,
                        price: 1000,
                        displayName: "poison wave tower",
                        sprites: [
                            "img/poison_tower_1.png"
                        ],
                        spriteInterval: 6,
                        attack: {
                            type: 'poison_wave',
                            damage: 8,
                            factor: 2,
                            range: 50,
                            interval: 70
                        }
                    }
                ]
            }
        });

        Towers.upsert({
            type: 'ice_tower'
        }, {
            $set: {
                levels: [
                    {
                        level: 0,
                        price: 700,
                        displayName: "ice wave tower",
                        sprites: [
                            "img/ice_tower_0.png"
                        ],
                        spriteInterval: 6,
                        attack: {
                            type: 'ice_wave',
                            range: 50,
                            factor: 2,
                            interval: 40
                        }
                    },
                    {
                        level: 1,
                        price: 900,
                        displayName: "ice wave tower",
                        sprites: [
                            "img/ice_tower_1.png"
                        ],
                        spriteInterval: 6,
                        attack: {
                            type: 'ice_wave',
                            range: 55,
                            factor: 3,
                            interval: 40
                        }
                    }

                ]
            }
        });
    });
}
