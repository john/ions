if (Meteor.isServer) {
    Meteor.startup(function () {

        Mobs.remove({});

        Mobs.upsert(
            {
                type: "enemy1"
            }, {
                $set: {
                    sprites: [
                        "img/en01.png"
                    ],
                    spriteInterval: 8,
                    speed: 1,
                    hp: 11,
                    price: 10,
                    value: 10
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy2"
            }, {
                $set: {
                    sprites: [
                        "img/en02.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.4,
                    hp: 14,
                    price: 13,
                    value: 13
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy3"
            }, {
                $set: {
                    sprites: [
                        "img/en03.png"
                    ],
                    spriteInterval: 8,
                    speed: 1,
                    hp: 18,
                    price: 15,
                    value: 15
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy4"
            }, {
                $set: {
                    sprites: [
                        "img/en04.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.4,
                    hp: 25,
                    price: 20,
                    value: 20
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy5"
            }, {
                $set: {
                    sprites: [
                        "img/en05.png"
                    ],
                    spriteInterval: 8,
                    speed: 1,
                    hp: 40,
                    price: 25,
                    value: 25
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy6"
            }, {
                $set: {
                    sprites: [
                        "img/en06.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.4,
                    hp: 55,
                    price: 26,
                    value: 26
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy7"
            }, {
                $set: {
                    sprites: [
                        "img/en07.png"
                    ],
                    spriteInterval: 8,
                    speed: 1,
                    hp: 75,
                    price: 31,
                    value: 31
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy8"
            }, {
                $set: {
                    sprites: [
                        "img/en08.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.4,
                    hp: 95,
                    price: 40,
                    value: 40
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy9"
            }, {
                $set: {
                    sprites: [
                        "img/en09.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.4,
                    hp: 130,
                    price: 45,
                    value: 45
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy10"
            }, {
                $set: {
                    sprites: [
                        "img/en10.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.2,
                    hp: 175,
                    price: 50,
                    value: 50
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy11"
            }, {
                $set: {
                    sprites: [
                        "img/en11.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.6,
                    hp: 200,
                    price: 55,
                    value: 55
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy12"
            }, {
                $set: {
                    sprites: [
                        "img/en12.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.2,
                    hp: 240,
                    price: 60,
                    value: 60
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy13"
            }, {
                $set: {
                    sprites: [
                        "img/en13.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.0,
                    hp: 250,
                    price: 65,
                    value: 65
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy14"
            }, {
                $set: {
                    sprites: [
                        "img/en14.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.4,
                    hp: 290,
                    price: 70,
                    value: 70
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy15"
            }, {
                $set: {
                    sprites: [
                        "img/en15.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.2,
                    hp: 320,
                    price: 75,
                    value: 75
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy16"
            }, {
                $set: {
                    sprites: [
                        "img/en16.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.0,
                    hp: 360,
                    price: 80,
                    value: 80
                }
            }
        );

        Mobs.upsert(
            {
                type: "enemy17"
            }, {
                $set: {
                    sprites: [
                        "img/en17.png"
                    ],
                    spriteInterval: 8,
                    speed: 1.5,
                    hp: 390,
                    price: 80,
                    value: 80
                }
            }
        );
    });
}
