/**
 * @typedef {Object} Mob
 * @property {String} type
 * @property {Number} health
 * @property {Number} price
 * @property {Number} speed
 * @property {String[]} spirtes
 */

Mobs = new Mongo.Collection("mobs");

if (Meteor.isClient) {
    /**
     * finds a walker definition by its type
     * @param {Mob.type} type
     * @return {Mob}
     */

    loadMobSprites = function () {
        var mobs = Mobs.find({});

        mobs.forEach(function (mob) {
            mob.sprites.forEach(function (sprite) {
                Game.sprite_pool.registerSprite(sprite);
            });
        });
    }

    mobSurvived = function (mob) {
        Meteor.call("mobSurvived", getPlayerId());
    }

    mobKilled = function (mob) {
        Meteor.call("mobKilled", getPlayerId(), mob.def.type);
    }
}

if (Meteor.isServer) {
    Meteor.methods({
        mobSurvived: function (player_id) {
            decrementPlayerHp(player_id, DECREMENT_LIVE_PER_MONSTER);
        },
        mobKilled: function (player_id, mob_type) {
            var mob = getMobByType(mob_type);
            changePlayerMoney(player_id, mob.value, 1);
        }
    });
}

getMobByType = function (type) {
    return Mobs.findOne({
        type: type
    });
}
