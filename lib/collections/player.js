/**
 * @typedef {Object} Player
 * @property {Meteor.Collection.ObjectID} _id
 * @property {String} email
 * @property {Number} money
 * @property {Number} health
 */

Players = new Mongo.Collection("players");

if (Meteor.isClient) {
    /**
     * @return {Player}
     */
    getPlayer = function () {
        return Players.findOne({
            _id: getPlayerId()
        });
    }

    getPlayerId = function () {
        return Session.get("player")._id;
    }

    getMoney = function () {
        return getPlayer().money;
    }

    getHP = function () {
        return getPlayer().hp;
    }

    getEnemyMoney = function () {
        return getEnemy().money;
    }

    getEnemyHP = function () {
        return getEnemy().hp;
    }

    getMatch = function () {
        return Matches.findOne({
            $or: [
                {player1: getPlayerId()},
                {player2: getPlayerId()}
            ]
        });
    }

    resetPlayer = function () {
        Meteor.call("resetPlayer", getPlayerId());
    }
}

if (Meteor.isServer) {
    Meteor.methods({
        insertOrLoginPlayer: function (email, password) {
            var player = Players.findOne({
                email: email
            });

            if (player != undefined) {
                if (player.password == password) {
                    Players.update(player._id, {
                        $set: {
                            money: PLAYER_START_MONEY,
                            hp: PLAYER_START_HP,
                            online: true,
                            inMatch: false,
                            hasEnemy: false,
                            lastUpdated: new Date()
                        }
                    });
                } else {
                    return null;
                }
            } else {
                Players.insert({
                    email: email,
                    password: password,
                    wins: 0,
                    looses: 0,
                    money: PLAYER_START_MONEY,
                    hp: PLAYER_START_HP,
                    online: true,
                    inMatch: false,
                    hasEnemy: false,
                    createdDate: new Date(),
                    lastUpdated: new Date()
                });
            }

            return getPlayerByEmail(email);
        },
        resetPlayer: function (player_id) {
            var player = getPlayerById(player_id);

            Players.update({
                _id: player._id
            }, {
                $set: {
                    money: 1000,
                    hp: 100,
                    lastUpdated: new Date()
                }
            });
        }

    });

    changePlayerMoney = function (player_id, value, dir) {
        var player = getPlayerById(player_id);

        Players.update({
            _id: player._id
        }, {
            $set: {
                money: player.money + (value * dir),
                lastUpdated: new Date()
            }
        });
    }

    decrementPlayerHp = function (player_id, value) {
        var player = getPlayerById(player_id);
        if (player.hp - value < 1) {
            playerLoosesGame(player_id);
        }
        Players.update({
            _id: player._id
        }, {
            $set: {
                hp: player.hp - value,
                lastUpdated: new Date()
            }
        });
    }

    incrementPlayerMoneyPeriodically = function (match_id) {
        var match = Matches.findOne(match_id);
        if (match) {
            Meteor.setTimeout(function () {
                incrementPlayerMoneyPeriodically(match_id);
            }, MONEY_INCREMENT_TIME * 1000);
            Players.update({
                $or: [
                    {_id: match.player1},
                    {_id: match.player2}
                ]
            }, {
                $inc: {
                    money: MONEY_INCREMENT_AMOUNT
                },
                $set: {
                    lastUpdated: new Date()
                }
            }, {
                multi: true
            });

        }
    }

    playerDisconnected = function (player_id) {
        console.log("player disconnected " + player_id);

        Players.update({
            _id: player_id
        }, {
            $set: {
                online: false,
                inMatch: false
            }
        });

        var match = Matches.findOne({
            $or: [
                {player1: player_id},
                {player2: player_id}
            ]
        });
        var enemy_id = (player_id == match.player1) ? match.player2 : match.player1;
        if (enemy_id == "" || match.winner != "") {
            Matches.remove(match._id);
        } else {
            playerLoosesGame(player_id);
        }
    }

    playerLoosesGame = function (player_id) {
        var match = Matches.findOne({
            $or: [
                {player1: player_id},
                {player2: player_id}
            ]
        });

        if (match.winner == "") {

            Players.update({
                _id: player_id
            }, {
                $inc: {
                    looses: 1
                }
            });


            var enemy_id = (player_id == match.player1) ? match.player2 : match.player1;
            Matches.update({
                $or: [
                    {player1: player_id},
                    {player2: player_id}
                ]
            }, {
                $set: {
                    winner: enemy_id
                }
            });

            Players.update(enemy_id, {
                $inc: {
                    wins: 1
                }
            });

            MobQueue.remove({
                $or: {
                    player_id: player_id,
                    player_id: enemy_id
                }
            });
        }
    }
}

getPlayerById = function (player_id) {
    return Players.findOne({
        _id: player_id
    });
}

getPlayerByEmail = function (email) {
    return Players.findOne({
        email: email
    });
}
