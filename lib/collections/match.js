/**
 * @typedef {Object} Match
 * @property {Player._id} player1
 * @property {Player._id} player2
 */

Matches = new Mongo.Collection("matches");

if (Meteor.isClient) {

    createMatch = function () {
        Meteor.call("createMatch", getPlayerId(), function (error, result) {
            Session.set("creator", true);
        });
    }

    joinMatch = function (match_id) {
        Meteor.call("joinMatch", getPlayerId(), match_id, function (error, result) {
            Session.set("creator", false);
        });
    }

    getMatch = function () {
        return Matches.findOne({
            $or: [
                {player1: getPlayerId()},
                {player2: getPlayerId()}
            ]
        })
    }

    /**
     * @return {Player}
     */
    getEnemy = function () {
        return getPlayerById((Session.get("creator")) ? getMatch().player2 : getMatch().player1);
    }
}

if (Meteor.isServer) {

    Meteor.methods({
        createMatch: function (player_id) {
            Players.update({
                _id: player_id
            }, {
                $set: {
                    inMatch: true
                }
            });

            Matches.upsert({
                $or: [
                    {player1: player_id},
                    {player2: player_id}
                ]
            }, {
                player1: player_id,
                player2: "",
                winner: ""
            });
        },
        joinMatch: function (player_id, match_id) {
            Matches.update({
                _id: match_id
            }, {
                $set: {
                    player2: player_id,
                    startedDate: new Date()
                }
            });

            var match = Matches.findOne(match_id);

            Players.update({
                $or: [
                    {_id: match.player1},
                    {_id: match.player2}
                ]
            }, {
                $set: {
                    inMatch: true,
                    hasEnemy: true
                }
            }, {
                multi: true
            });

            Meteor.setTimeout(function () {
                incrementPlayerMoneyPeriodically(match_id);
            }, MONEY_INCREMENT_TIME);
        }
    })
    ;
}
