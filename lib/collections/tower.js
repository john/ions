/**
 * @typedef {Object} Level
 * @property {Number} level
 * @property {Number} price
 * @property {Number} range
 * @property {Number} damage
 * @property {Number} speed
 * @property {String} display_name
 * @property {String[]} sprites
 * @property {Number} interval
 * @property {Number} rotation_steps
 */

/**
 * @typedef {Object} Tower
 * @property {String} type
 * @property {Level[]} levels
 */

Towers = new Mongo.Collection("towers");

if (Meteor.isClient) {
    /**
     * @param {Tower} tower
     */
    buildTower = function (tower) {
        Meteor.call(
            "buildTower",
            tower.type,
            0,
            getPlayerId(),
            function (error, result) {
                Session.set("towerBuyMenuVisible", false);
            }
        );
    }

    /**
     * @param {Tower} tower
     */
    upgradeTower = function (tower) {
        Meteor.call(
            "buildTower",
            tower.def.type,
            tower.level + 1,
            getPlayerId(),
            function (error, result) {
                if (result) {
                    Game.upgradeTower();
                }
                Game.selected = false;
                Session.set("towerUpgradeMenuVisible", false);
            }
        );
    }

    /**
     * @param {Tower} tower
     */
    sellTower = function (tower) {
        Meteor.call(
            "sellTower",
            tower.def.type,
            tower.level,
            getPlayerId(),
            function (error, result) {
                if (result) {
                    Game.sellTower();
                }
                Game.selected = false;
                Session.set("towerUpgradeMenuVisible", false);
            }
        );
    }

    loadTowerSprites = function () {
        var towers = Towers.find({});

        towers.forEach(function (tower) {
            tower.levels.forEach(function (level) {
                if (level.rotation_steps > 0) {
                    level.sprites.forEach(function (sprite) {
                        Game.sprite_pool.registerRotatingSprite(sprite, level.rotation_steps);
                    });
                } else {
                    level.sprites.forEach(function (sprite) {
                        Game.sprite_pool.registerSprite(sprite);
                    });
                }
            });
        });
    }

    getTower = function (type) {
        return Towers.findOne({
            type: type
        });
    }

}

if (Meteor.isServer) {

    /**
     * @param {Tower.type} tower_type
     */
    Meteor.methods({
        buildTower: function (tower_type, tower_level, player_id) {
            var tower = Towers.findOne({
                type: tower_type
            }).levels[tower_level];
            var player = getPlayerById(player_id);

            if (player.money >= tower.price) {

                changePlayerMoney(player_id, tower.price, -1);

                return true;
            }

            return false;
        },
        sellTower: function (tower_type, tower_level, player_id) {
            var tower = getTower(tower_type);
            var sell_value = calcSellPrice(tower, tower_level);

            changePlayerMoney(player_id, sell_value, 1);

            return true;
        }
    });
}

getTower = function (type) {
    return Towers.findOne({
        type: type
    });
}

calcSellPrice = function (tower, level) {
    var value = 0;

    tower.levels.forEach(function (elem, key) {
        if (key <= level) {
            value += elem.price;
        }
    });

    return Math.round(value * TOWER_SELL_FACTOR);

}
