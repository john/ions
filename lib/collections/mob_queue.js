/**
 * @typedef {Object} MobQueue
 * @property {Mob.type} mob_type
 * @property {Player._id} player_id
 * @property {Number} count
 * @property {Number} timestamp
 */

MobQueue = new Mongo.Collection("mob_queue");

if (Meteor.isClient) {
    /**
     * @return {Mob[]} gives an array of {MobQueue.count} Mobs
     */
    popMobFromQueue = function () {
        var mobQueueEntry;

        mobQueueEntry = MobQueue.findOne({}, {
            $sort: {
                timestamp: 1 // ASC
            }
        });

        if (mobQueueEntry && (mobQueueEntry._id != Session.get("lastMobQueueId"))) {
            Session.set("lastMobQueueId", mobQueueEntry._id);
            Meteor.call("deleteMobQueueEntry", mobQueueEntry._id);
        } else {
            mobQueueEntry = null;
        }

        return mobQueueEntry;
    }

    /**
     * @param {Mob} mob
     * @param {Number} count
     */
    sendMobToEnemy = function (mob_type, count) {
        console.log("send mob");
        var enemy_id = (Session.get('singleplayer')) ? getPlayerId() : getEnemy()._id;
        Meteor.call("sendMobToEnemy", getPlayerId(), mob_type, count, enemy_id, Session.get('singleplayer'));
    }
}

if (Meteor.isServer) {

    Meteor.methods({
        sendMobToEnemy: function (player_id, mob_type, count, enemy_id, free) {
            var mob = getMobByType(mob_type);
            var cost = mob.price * count;
            if (getPlayerById(player_id).money >= cost || free) {
                MobQueue.insert({
                    mob_type: mob_type,
                    count: count,
                    player_id: enemy_id,
                    timestamp: new Date()
                })

                if (!free) changePlayerMoney(player_id, cost, -1);
            }
        },
        deleteMobQueueEntry: function (object_id) {
            MobQueue.remove({
                _id: object_id
            });
        }
    });
}
