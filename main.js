if (Meteor.isClient) {
    // This code only runs on the client
    Template.main.helpers({
        loggedIn: function () {
            return Session.get("loggedIn") != undefined;
        },
        loading: function () {
            return (Session.get("loading") || !(Session.get("subscriptions_left") < 1));
        },
        subscriptionsLoaded: function () {
            return (Session.get("subscriptions_left") < 1);
        },
        searchingEnemy: function () {
            return !(getPlayer().hasEnemy || Session.get('singleplayer'));
        },
        isSingleplayer: function () {
            return Session.get('singleplayer');
        },
        inMatch: function () {
            return getPlayer().inMatch || Session.get('singleplayer');
        },
        isMatchFinished: function () {
            return getPlayer().inMatch && getMatch().winner != "";
        }
    });

    subscribeAll = function () {
        Session.set("loading", true);
        Session.set("subscriptions_left", NUMBER_OF_SUBSCRIPTIONS);

        Meteor.subscribe("players", subscriptionLoaded());
        Meteor.subscribe("matches", getPlayerId(), subscriptionLoaded());
        Meteor.subscribe("mobs", subscriptionLoaded());
        Meteor.subscribe("mob_queue", getPlayerId(), subscriptionLoaded());
        Meteor.subscribe("towers", subscriptionLoaded());

    }

    subscriptionLoaded = function () {
        return function () {
            console.log("subscription loaded");
            Session.set("subscriptions_left", Session.get("subscriptions_left") - 1);
        }
    }

    restartGame = function (player_id) {
        Meteor.call("restartGame", player_id);
    }

    Template.main.events({
        'touchmove *': function (event, template) {
            var e = event.originalEvent;
            var y = $('body').scrollTop();
            var dir = e.changedTouches[0].pageY > Session.get('last_y') ? 1 : -1;
            Session.set('last_y', e.changedTouches[0].pageY);
            console.log(y + ' | ' + Session.get('last_y') + ' | ' + dir);
            if (dir > 0 && y < 2) {
                event.preventDefault();
            }
        }
    });
}

if (Meteor.isServer) {
    Meteor.methods({
        restartGame: function (player_id) {
            Players.update(player_id, {
                $set: {
                    money: PLAYER_START_MONEY,
                    hp: PLAYER_START_HP,
                    inMatch: false,
                    hasEnemy: false
                }
            });
        }
    });
}
